(use-modules (guix packages)
             (guix gexp)
             ((guix licenses) #:select (gpl3+))
             (guix build-system emacs))

(package
 (name "emacs-headstrong-theme")
 (version "0.1")
 (source (local-file "headstrong-theme.el"))
 (build-system emacs-build-system)
 (home-page "https://gitlab.com/lilyp/emacs-headstrong-theme")
 (synopsis "Flip your mode-line into the header")
 (description "Headstrong provides a setup to move your mode-line into the
header-line, regardless of what other customizations and themes you might have
enabled.")
 (license gpl3+))
