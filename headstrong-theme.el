;;; headstrong-theme.el -- Flip your mode-line into the header.

;; Copyright © 2022 Liliana Marie Prikler <liliana.prikler@gmail.com>
;; Version: 0.1

;; This theme is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This theme is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this theme.  If not, see <https://www.gnu.org/licenses/>.

;; Commentary:

;; Headstrong provides a "minimal", "composable" setup to move your mode-line
;; into the header-line, regardless of what other customizations and themes
;; you might have enabled.

;;; Code:

;;;###autoload (add-to-list 'custom-theme-load-path default-directory)
(deftheme headstrong
  "Flips your mode-line into the header-line.")

(put 'header-line-format 'standard-value '(nil))

(defun headstrong--mode-line->header-line (construct)
  (pcase construct
    ((and (pred keymapp) keymap)
     (let* ((map (copy-keymap keymap)))
       (define-key map [mode-line] nil)
       (define-key map [header-line]
         (lookup-key keymap [mode-line]))
       map))
    ((pred stringp)
     (let ((string (copy-sequence construct)))
       (alter-text-property 0 (length (substring-no-properties construct))
                            'keymap
                            #'headstrong--mode-line->header-line
                            string)
       (alter-text-property 0 (length (substring-no-properties construct))
                            'local-map
                            #'headstrong--mode-line->header-line
                            string)
       string))
    (`(:propertize ,elt . ,props)
     `(:propertize ,elt . ,(mapcar #'headstrong--mode-line->header-line props)))
    ((pred listp)
     (mapcar #'headstrong--mode-line->header-line construct))
    ;; We don't know what to do, so let's do nothing instead.
    (_ construct)))

(defvar headstrong-mode-line-advice)
(put 'headstrong-mode-line-advice 'variable-documentation
     "Whether to actually convert mode line constructs to equivalent header
line constructs.  If unbound, check whether the `headstrong' theme is currently
enabled instead.")
(defun headstrong-mode-line-advice (construct)
  "Convert CONSTRUCT to an equivalent header line construct if the header line
is used to hold the mode line."
  (when (if (boundp 'headstrong-mode-line-advice)
            (symbol-value 'headstrong-mode-line-advice)
          (custom-theme-enabled-p 'headstrong))
    (headstrong--mode-line->header-line construct)))

(defcustom headstrong-header-line
  (list
   "%e"
   mode-line-front-space
   (headstrong--mode-line->header-line mode-line-mule-info)
   mode-line-client
   (headstrong--mode-line->header-line mode-line-modified)
   mode-line-remote
   mode-line-frame-identification
   mode-line-buffer-identification
   "   "
   (headstrong--mode-line->header-line mode-line-position)
   '(vc-mode (:eval (headstrong--mode-line->header-line vc-mode)))
   "  " (headstrong--mode-line->header-line mode-line-modes)
   'mode-line-misc-info mode-line-end-spaces)
  "Like `mode-line-format', but adjusted for header line use."
  :set (lambda (sym val)
         (set-default sym val)
         (custom-theme-recalc-variable 'header-line-format)))

(eval-after-load "org-clock"
  `(progn
     (unless (get 'org-clock-mode-line-map 'standard-value)
       (put 'org-clock-mode-line-map 'standard-value
            `((quote ,org-clock-mode-line-map)))
       (put 'org-clock-mode-line-map 'force-value t))
     (let ((custom--inhibit-theme-enable
            (not (custom-theme-enabled-p 'headstrong))))
       (custom-push-theme 'theme-value 'org-clock-mode-line-map
                          'headstrong 'set
                          `(quote ,(headstrong--mode-line->header-line
                                    org-clock-mode-line-map)))
       (unless custom--inhibit-theme-enable ;; Why???
         (custom-theme-recalc-variable 'org-clock-mode-line-map)))))

(let ((fg (face-foreground 'default))
      (bg (face-background 'default))
      (neutral "#999999"))
  (custom-theme-set-variables
   'headstrong
   `(header-line-format headstrong-header-line)
   '(mode-line-format '("")))

  (custom-theme-set-faces
   'headstrong
   `(mode-line ((t (:height 1 :underline ,fg :foreground ,fg :background ,bg))))
   `(mode-line-inactive ((t (:height 1 :underline ,neutral :foreground ,fg :background ,bg))))
   `(org-mode-line-clock ((t (:inherit header-line))))
   `(org-mode-line-clock-overrun ((t (:inherit header-line))))))

(provide-theme 'headstrong)
